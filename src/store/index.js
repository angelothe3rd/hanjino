import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import { addDataToFirestore, getDataFromFirestore, getKana } from '@/firebase/function';

export default new Vuex.Store({
  state: {
    kanji: [],
    kana: []
  },
  getters: {
     hiragana_seion: function(state){
       return state.kana[0];
     },
     katakana_seion: function(state){
       return state.kana[1];
     },
     characters: function(state){
        return state.kanji.map((x) => {
          return {
            key: x.key,
            character: x.character, 
            meaning: x.meaning, 
            readingFirst: x.reading.split("、")[0],
            reading: x.reading,
            korean: x.korean,
            tagalog: x.tagalog,
            isEditing: x.isEditing
          }
        })
     },
     getCount: function(state) {
       return state.kanji.length;
     }
  },
  mutations: {
    ADD_DATA(state, data){
       addDataToFirestore("kanji", data);
    },
    GET_DATA(state, collection){
      getDataFromFirestore(state, collection);
    },
    GET_KANA(state, collection){
       getKana(state, collection); 
    },
  },
  actions: {
    addKanji({ commit }, data){
      commit("ADD_DATA", data);
    },
    getKanji({ commit }, collection){
      commit("GET_DATA", collection);
    },
    getKana({ commit }, collection) {
      commit("GET_KANA", collection);
    }
  },
  modules: {
  }
})
