import { credential } from '@/firebase/firebase';
 
let createAccount = function(email, password){
    credential.createUserWithEmailAndPassword(email, password).catch(function(){
        console.log(error);
        // then get redirect to home
    })
}

let signIn = function(email, password) {
    credential.signInWithEmailAndPassword(email, password).catch(function(error){
        console.log(error);
        // then get redirect to home
    })
}

let signOut = function(){
    credential.signOut().then(function(){
        // Sign out successful
    }, function(){
        console.log(error);
    })
}

let removeUser = function(email, password) {
    firebase.removeUser({
        email: email, 
        password: password       
    }, function(error){
        if(error == null){
            console.log("User remove user successfully!");
        }
        else {
            console.log("Error removing user!")
        }
    })
}

export {
    createAccount, 
    removeUser,
    signIn,
    signOut
}