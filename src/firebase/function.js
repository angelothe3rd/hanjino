import {  firestore } from '../firebase/firebase';

const addData = function(collection, data) {
    let ref = firestore.collection(collection);
    ref.doc(data.character).set(data)
        .then(function() {
        console.log("Data posted!");
    })
    .catch(function(error) {
        console.error("Error: ", error);
    });
}


let getDataFromFirestore = (state, collection) => {

    let ref = firestore.collection(collection);
    let descendingOrder = ref.orderBy("timestamp", "desc");
    descendingOrder.onSnapshot({
          includeMetadataChanges: false
       }, function(querySnapshot) {
     
    var list = [];
        querySnapshot.forEach(function(doc) {

            let { character, meaning, reading, korean, tagalog } = doc.data();
            
            list.push({
                key: doc.id,
                character: character, 
                meaning: meaning,
                reading: reading,
                korean: korean,
                tagalog: tagalog,
                isEditing: false
            });
    
        })
        state.kanji = list;
   
    })

}


let addDataToFirestore = (collection, data) => {
    let timestamp = Date.now();
    addData(collection, { ...data, timestamp})
}


const addKana = function(collection, data, docname) {
    let ref = firestore.collection(collection);
    ref.doc(docname).set(data, { merge: true}).then(function() {
        console.log("Data posted!");
    })
    .catch(function(error) {
        console.error("Error: ", error);
    });
}

const getKana = function(state, collection) {
    let ref = firestore.collection(collection);
    ref.onSnapshot(function(snapshot){
        var list = [];
        snapshot.forEach(function(doc) {
            let { seiOn } = doc.data();
            list.push(seiOn);
        })
        state.kana = list;
    })
}

export { 
    addDataToFirestore, 
    getDataFromFirestore, 
    addKana, 
    getKana
}