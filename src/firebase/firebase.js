import firebase from 'firebase/app';
import 'firebase/firebase-auth';
import 'firebase/firebase-firestore';


const firebaseConfig = {
    apiKey: "AIzaSyBHqE4gBtbI1Xh6IL8VcLZ_Cq0GH6wVJac",
    authDomain: "hanjino.firebaseapp.com",
    databaseURL: "https://hanjino.firebaseio.com",
    projectId: "hanjino",
    storageBucket: "hanjino.appspot.com",
    messagingSenderId: "468363288179",
    appId: "1:468363288179:web:3b2301900bcbca4ac51406",
    measurementId: "G-N4XDDK73G5"
};

firebase.initializeApp(firebaseConfig);

const credential = firebase.auth();
const firestore = firebase.firestore();

// firestore.settings({ timestampsInSnapshots: true })

export { credential, firestore }