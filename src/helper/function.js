const clear = function(obj){
    Object.keys(obj).filter(function(k){
       return obj[k] = "";
    })
}

let checkFormHaveEmptyValues = function(object) {

  const keysArray = Object.keys(object);
  // Get all object keys and put them in an array
  const valuesArray = Object.values(object);
  // Get all object values and put them in an array

  const valuesFilter = valuesArray.filter((x) => {
    if(x !== ""){
      return x;
    }
  })

  let checkIfKeysMatchesWithValues = function(){
    return keysArray.length !== valuesFilter.length ? false : true;
    // Compare two arrays length, return true if match
  }

  if(checkIfKeysMatchesWithValues()){
    return object;
  }
  else {
    alert("Please fill in blank!");
  }
 
}


export {
  clear,
  checkFormHaveEmptyValues
}