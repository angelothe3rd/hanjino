import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/pages/home.vue';
import Kana from '@/pages/kana.vue';
import Login from '@/pages/login.vue';
import Kanji from '@/pages/kanji.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'root',
    redirect: {
      name: 'home'
    }
  },
  {
    path: '/home/',
    name: 'home',
    component: Home,
  },
  {
    path: '/kana/:kana',
    name: 'katakana',
    component: Kana
  },
  {
    path: '/kanji/:character',
    name: 'kanji',
    component: Kanji
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  }

]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
